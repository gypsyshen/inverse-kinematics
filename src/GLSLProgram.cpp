/*
Implementation of class GLSLProgram.
*/

#include "GLSLProgram.h"

void GLSLProgram::Init() {
	// Create and compile our GLSL program from the shaders
	programID = LoadShaders("BRDF.vertexshader", "BRDF.fragmentshader");

	// Get a handle for our "MVP" uniform
	MatrixID = glGetUniformLocation(programID, "MVP");
	ViewMatrixID = glGetUniformLocation(programID, "V");
	ModelMatrixID = glGetUniformLocation(programID, "M");

	// Get a handle for our "material" uniform
	MaterialDiffuseID = glGetUniformLocation(programID, "Material.diffuse");
	MaterialAmbientID = glGetUniformLocation(programID, "Material.ambient");
	MaterialSpecularID = glGetUniformLocation(programID, "Material.specular");

	// Get a handle for our "LightInvDirection_worldspace" uniform
	lightInvDirID = glGetUniformLocation(programID, "LightInvDirection_worldspace");
}

void GLSLProgram::DrawGeometry(const GLuint &vertexbuffer, const GLuint &normalbuffer, const GLuint &elementbuffer,
	const vector<unsigned short> &indices) const {

	// 1rst attribute buffer : vertices
	glEnableVertexAttribArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, vertexbuffer);
	glVertexAttribPointer(
		0,                  // attribute
		3,                  // size
		GL_FLOAT,           // type
		GL_FALSE,           // normalized?
		0,                  // stride
		(void*)0            // array buffer offset
		);

	// 2rd attribute buffer : normals
	glEnableVertexAttribArray(1);
	glBindBuffer(GL_ARRAY_BUFFER, normalbuffer);
	glVertexAttribPointer(
		1,                                // attribute
		3,                                // size
		GL_FLOAT,                         // type
		GL_FALSE,                         // normalized?
		0,                                // stride
		(void*)0                          // array buffer offset
		);

	// Index buffer
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, elementbuffer);

	// Draw the triangles !
	glDrawElements(
		GL_TRIANGLES,      // mode
		indices.size(),    // count
		GL_UNSIGNED_SHORT, // type
		(void*)0           // element array buffer offset
		);

	glDisableVertexAttribArray(0);
	glDisableVertexAttribArray(1);
}


void GLSLProgram::RenderToScreen(int window_size_w, int window_size_h,
	const Joint * J0, const vector<vec3> &jointPositions, const vec3 &G,
	const mat4 &VP, const mat4 &ViewMatrix,
	const glm::vec3 lightInvDir,
	const Mesh &mesh_sphere, const Mesh &mesh_goal, const Mesh &mesh_axis) const {

	glBindFramebuffer(GL_FRAMEBUFFER, 0);
	glViewport(0, 0, window_size_w, window_size_h); // Render on the whole framebuffer, complete from the lower left corner to the upper right

	glEnable(GL_CULL_FACE);
	glCullFace(GL_BACK); // Cull back-facing triangles -> draw only front-facing triangles

	// Clear the screen
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	// Use our shader
	glUseProgram(programID);

	glUniform3f(lightInvDirID, lightInvDir.x, lightInvDir.y, lightInvDir.z);

	// Send our transformation to the currently bound shader,
	// in the "MVP" uniform
	glUniformMatrix4fv(ViewMatrixID, 1, GL_FALSE, &ViewMatrix[0][0]);

//*	// draw the x-y-z axis
	{
		// x axis
		glUniform3f(MaterialSpecularID, 1.f, 0.f, 0.f);
		glUniform3f(MaterialAmbientID, 1.f * 0.2f, 0.f, 0.f);
		glUniform3f(MaterialDiffuseID, 1.f, 0.f, 0.f);

		mat4 ModelMatrix = rotate(-90.f, 0.f, 0.f, 1.f) * scale(0.5f, 0.5f, 0.5f);
		mat4 MVP = VP * ModelMatrix;

		glUniformMatrix4fv(MatrixID, 1, GL_FALSE, &MVP[0][0]);
		glUniformMatrix4fv(ModelMatrixID, 1, GL_FALSE, &ModelMatrix[0][0]);

		DrawGeometry(mesh_axis.vertexbuffer, mesh_axis.normalbuffer, mesh_axis.elementbuffer, mesh_axis.indices);


		// y axis
		glUniform3f(MaterialSpecularID, 0.f, 1.f, 0.f);
		glUniform3f(MaterialAmbientID, 0.f, 1.f * 0.2f, 0.f);
		glUniform3f(MaterialDiffuseID, 0.f, 1.f, 0.f);

		ModelMatrix = scale(0.5f, 0.5f, 0.5f);
		MVP = VP * ModelMatrix;

		glUniformMatrix4fv(MatrixID, 1, GL_FALSE, &MVP[0][0]);
		glUniformMatrix4fv(ModelMatrixID, 1, GL_FALSE, &ModelMatrix[0][0]);

		DrawGeometry(mesh_axis.vertexbuffer, mesh_axis.normalbuffer, mesh_axis.elementbuffer, mesh_axis.indices);


/*		// z axis
		glUniform3f(MaterialSpecularID, 0.f, 0.f, 1.f);
		glUniform3f(MaterialAmbientID, 0.f, 0.f, 1.f * 0.2f);
		glUniform3f(MaterialDiffuseID, 0.f, 0.f, 1.f);

		ModelMatrix = rotate(90.f, 1.f, 0.f, 0.f) * scale(0.5f, 1.f, 0.5f);
		MVP = VP * ModelMatrix;

		glUniformMatrix4fv(MatrixID, 1, GL_FALSE, &MVP[0][0]);
		glUniformMatrix4fv(ModelMatrixID, 1, GL_FALSE, &ModelMatrix[0][0]);

		DrawGeometry(mesh_axis.vertexbuffer, mesh_axis.normalbuffer, mesh_axis.elementbuffer, mesh_axis.indices); */
	}

	// draw the goal
	{
		glUniform3f(MaterialSpecularID, 0.8f, 0.2f, 0.2f);
		glUniform3f(MaterialAmbientID, 0.8f * 0.2f, 0.2f * 0.2f, 0.2f * 0.2f);
		glUniform3f(MaterialDiffuseID, 0.8f, 0.2f, 0.2f);

		mat4 ModelMatrix = translate(G) * scale(0.4f, 0.4f, 0.4f);
		mat4 MVP = VP * ModelMatrix;

		glUniformMatrix4fv(MatrixID, 1, GL_FALSE, &MVP[0][0]);
		glUniformMatrix4fv(ModelMatrixID, 1, GL_FALSE, &ModelMatrix[0][0]);

		// Draw
		DrawGeometry(mesh_goal.vertexbuffer, mesh_goal.normalbuffer, mesh_goal.elementbuffer, mesh_goal.indices);
	}

	// draw the links and joints

	glUniform3f(MaterialSpecularID, 0.3f, 0.3f, 0.3f);
	glUniform3f(MaterialAmbientID, 80.f / 255.f * 0.5f, 168.f / 255.f * 0.5f, 227.f / 255.f * 0.5f);
	glUniform3f(MaterialDiffuseID, 80.f / 255.f, 168.f / 255.f, 227.f / 255.f);

	const Joint * J = J0;
	mat4 r = mat4(1.f);
	vector<vec3>::const_iterator p = jointPositions.begin();
	while (J) {

		// draw the link
		mat4 bias = translate(0.5f, 0.f, 0.f);
		r = rotate(J->localTheta, 0.f, 0.f, 1.f) * r;
		mat4 s = scale(J->length, 0.1f, 0.1f);
		mat4 t = translate((*p).x, (*p).y, (*p).z);
		mat4 ModelMatrix = t * r * s * bias;
		mat4 MVP = VP * ModelMatrix;

		glUniformMatrix4fv(MatrixID, 1, GL_FALSE, &MVP[0][0]);
		glUniformMatrix4fv(ModelMatrixID, 1, GL_FALSE, &ModelMatrix[0][0]);

		DrawGeometry(mesh_sphere.vertexbuffer, mesh_sphere.normalbuffer, mesh_sphere.elementbuffer, mesh_sphere.indices);

		// draw the joint
		s = scale(0.4f, 0.4f, 0.4f);
		ModelMatrix = t * s;
		MVP = VP * ModelMatrix;

		glUniformMatrix4fv(MatrixID, 1, GL_FALSE, &MVP[0][0]);
		glUniformMatrix4fv(ModelMatrixID, 1, GL_FALSE, &ModelMatrix[0][0]);

		DrawGeometry(mesh_sphere.vertexbuffer, mesh_sphere.normalbuffer, mesh_sphere.elementbuffer, mesh_sphere.indices);

		// update the joint
		J = J->child;
		p++;
	}

	// draw the end effector
	mat4 t = translate((*p).x, (*p).y, (*p).z);
	mat4 s = scale(0.4f, 0.4f, 0.4f);
	mat4 ModelMatrix = t * s;
	mat4 MVP = VP * ModelMatrix;

	glUniformMatrix4fv(MatrixID, 1, GL_FALSE, &MVP[0][0]);
	glUniformMatrix4fv(ModelMatrixID, 1, GL_FALSE, &ModelMatrix[0][0]);

	DrawGeometry(mesh_sphere.vertexbuffer, mesh_sphere.normalbuffer, mesh_sphere.elementbuffer, mesh_sphere.indices);
}

void GLSLProgram::CleanUp() {
	glDeleteProgram(programID);
}