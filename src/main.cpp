/*
main file.
*/

// Include common headers
#include "Header.h"
#include "GLSLProgram.h"
#include "Joint.h"
#include "IK.h"
#include "Mesh.h"

// Global variables

GLFWwindow* window;

int window_size_w = 1024;
int window_size_h = 768;

// Entry point
int main(void) {

	// Initialise GLFW
	if (!glfwInit()) {

		fprintf(stderr, "Failed to initialize GLFW\n");
		return -1;
	}

	glfwWindowHint(GLFW_SAMPLES, 4);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

	// Open a window and create its OpenGL context
	window = glfwCreateWindow(window_size_w, window_size_h, "Real-time Soft Shadow", NULL, NULL);
	if (window == NULL) {
		fprintf(stderr, "Failed to open GLFW window. If you have an Intel GPU, they are not 3.3 compatible. Try the 2.1 version of the tutorials.\n");
		glfwTerminate();
		return -1;
	}
	glfwMakeContextCurrent(window);

	// Initialize GLEW
	glewExperimental = true; // Needed for core profile
	if (glewInit() != GLEW_OK) {
		fprintf(stderr, "Failed to initialize GLEW\n");
		return -1;
	}

	// Ensure we can capture the escape key being pressed below
	glfwSetInputMode(window, GLFW_STICKY_KEYS, GL_TRUE);
	glfwSetCursorPos(window, window_size_w * 0.5f, window_size_h * 0.5f);

	// Dark blue background
	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);

	// Enable depth test
	glEnable(GL_DEPTH_TEST);

	// Accept fragment if it closer to the camera than the former one
	glDepthFunc(GL_LESS);

	// Cull triangles which normal is not towards the camera
	glEnable(GL_CULL_FACE);

	// TODO: what's this?
	GLuint VertexArrayID;
	glGenVertexArrays(1, &VertexArrayID);
	glBindVertexArray(VertexArrayID);


	// ---------------- Shader programs for shadows ----------------
	GLSLProgram * brdf = new GLSLProgram();
	brdf->Init();

	// ---------------- Load the .obj files ----------------
	Mesh mesh_sphere;
	mesh_sphere.LoadMesh("sphere.obj");

	Mesh mesh_goal;
	mesh_goal.LoadMesh("bunny.obj");

	Mesh mesh_axis;
	mesh_axis.LoadMesh("axis.obj");


	// ---------------------------------------------
	// Initialize the joints
	// ---------------------------------------------
	//	Joint * J3 = new Joint(2.f, 45.f, NULL);
	Joint * J2 = new Joint(1.f, 30.f, NULL);
	Joint * J1 = new Joint(4.f, -60.f, J2);
	Joint * J0 = new Joint(2.f, 30.f, J1);

	// Update joint position
	vector<vec3> jointPositions;
	vec2 rootPosition = vec2(0.f, 0.f);
	{
		vec3 p = vec3(rootPosition.x, rootPosition.y, 0.f);
		jointPositions.push_back(p);
		float worldTheta = 0.f;
		const Joint * J = J0;
		while (J) {

			worldTheta += 3.14f * J->localTheta / 180.f;
			p = p + J->length * vec3(cos(worldTheta), sin(worldTheta), 0.f);
			jointPositions.push_back(p);

			// update the joint
			J = J->child;
		}
	}
	

	// ---------------------------------------------
	// main loop
	// ---------------------------------------------
	IK ik;
	do{
		// Update spot light
		computeSpotLightMatricesFromInputes();

		// Update local joint angel
		vec3 G = getLightPosition();
		G.z = 0.f;
		ik.Solve(G, jointPositions, J0, J1, J2);

		// Update joint position
		jointPositions.clear();
		vec3 p = vec3(rootPosition.x, rootPosition.y, 0.f);
		jointPositions.push_back(p);
		float worldTheta = 0.f;
		const Joint * J = J0;
		while (J) {

			worldTheta += 3.14f * J->localTheta / 180.f;
			p = p + J->length * vec3(cos(worldTheta), sin(worldTheta), 0.f);
			jointPositions.push_back(p);

			// update the joint
			J = J->child;
		}

		// --------------- Render to screen ---------------

		// Compute the MVP matrix from keyboard and mouse input
		computeMatricesFromInputs();
		glm::mat4 ProjectionMatrix = getProjectionMatrix();
		glm::mat4 ViewMatrix = getViewMatrix();
		
		mat4 VP = ProjectionMatrix * ViewMatrix;

		glm::vec3 lightInvDir = vec3(0.f, 1.f, 1.f); // getLightInvDirection();

		brdf->RenderToScreen(
			window_size_w, window_size_h,
			J0, jointPositions, G,
			VP, ViewMatrix,
			lightInvDir,
			mesh_sphere, mesh_goal, mesh_axis);

		// Save current screen if ' ' is pressed
		saveScreenToImage();

		// Swap buffers
		glfwSwapBuffers(window);
		glfwPollEvents();

	} // Check if the ESC key was pressed or the window was closed
	while (glfwGetKey(window, GLFW_KEY_ESCAPE) != GLFW_PRESS &&
	glfwWindowShouldClose(window) == 0);

	// Cleanup VBO and shader
	mesh_sphere.CleanUp();
	mesh_goal.CleanUp();
	mesh_axis.CleanUp();

	brdf->CleanUp();

	// Close OpenGL window and terminate GLFW
	glfwTerminate();

	return 0;
}