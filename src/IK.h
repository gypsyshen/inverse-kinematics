#ifndef IK_H
#define IK_H

#include "Header.h"
#include "Joint.h"

class IK {
public:
	void Solve(const vec3 &G, const vector<vec3> &jointPositions, Joint * J0, Joint * J1, Joint * J2) const;
};

#endif