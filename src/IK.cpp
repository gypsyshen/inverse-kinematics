#include "IK.h"

void IK::Solve(const vec3 &G, const vector<vec3> &jointPositions, Joint * J0, Joint * J1, Joint * J2) const {
	/*
	Solve IK.
	*/
	const vec3 E = jointPositions.at(3);
	const vec3 P2 = jointPositions.at(2);
	const vec3 P1 = jointPositions.at(1);
	vec3 V = G - E;

	vec3 axis = vec3(0.f, 0.f, 1.f);
	vec3 J_a = cross(axis, E);
	vec3 J_b = cross(axis, E - P1);
	vec3 J_c = cross(axis, E - P2);
	mat3 J = mat3(J_a, J_b, J_c);

	mat3 transJ = transpose(J);

	mat3 JtransJ = J * transJ;
	mat3 BiasJtransJ = JtransJ + 1.f * mat3(1.f);

	mat3 I = mat3(1.f);

	mat3 InvBiasJtransJ = inverse(BiasJtransJ);

	vec3 theta = transJ * InvBiasJtransJ * V;

	J0->localTheta += theta.x;
	J1->localTheta += theta.y;
	J2->localTheta += theta.z;

/*	// test
	static int count = 0;
	if (count == 0) {
		count++;
		cout << "\nJ_a: " << J_a.x << ", " << J_a.y << ", " << J_a.z << endl;
		cout << "\nJ_b: " << J_b.x << ", " << J_b.y << ", " << J_b.z << endl;
		cout << "\nJ_c: " << J_c.x << ", " << J_c.y << ", " << J_c.z << endl;
		cout << "\nIdentity matrix: " << endl;
		for (int i = 0; i < 3; ++i) {
			for (int j = 0; j < 3; ++j) {
				cout << I[i][j] << ", ";
			}
			cout << endl;
		}
		cout << "\nJ: " << endl;
		for (int i = 0; i < 3; ++i) {
			for (int j = 0; j < 3; ++j) {
				cout << J[i][j] << ", ";
			}
			cout << endl;
		}
		cout << "\nTranspose of J: " << endl;
		for (int i = 0; i < 3; ++i) {
			for (int j = 0; j < 3; ++j) {
				cout << transJ[i][j] << ", ";
			}
			cout << endl;
		}

		cout << "\nDeterminant of (J * J^T + beta^2 * I): " << determinant(BiasJtransJ) << endl;

		cout << "\nTheta: " << theta.x << ", " << theta.y << ", " << theta.z << endl;
	} */
}