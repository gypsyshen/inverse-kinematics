// Include GLFW
#include <glfw3.h>
extern GLFWwindow* window; // The "extern" keyword here is to access the variable "window" declared in tutorialXXX.cpp. This is a hack to keep the tutorials simple. Please avoid this.

// Include GLM
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
using namespace glm;

#include "controls.hpp"

////////////////////////////////////////////////////////////
/// save current screenshot
////////////////////////////////////////////////////////////
//#include <fstream>
#include <vector>
#include <iostream>
#include <ctime>
#include <string.h>

#include "lodepng.hpp"

using namespace std;

void saveScreenToImage() {

	if (glfwGetKey(window, GLFW_KEY_SPACE) == GLFW_PRESS) {
		
		int w = 1024;
		int h = 768;

		// 1. read screen pixels

		int w_mul_3 = w * 3;

		int buffersize = h * w_mul_3;
		unsigned char * buffer = new unsigned char[buffersize];
		glReadPixels(0, 0, w, h, GL_RGB, GL_UNSIGNED_BYTE, buffer);
		for (int j = 0; j < h; j++) {
			int a = j;
			int b = h - 1 - j;
			if (a >= b) break;
			int base_a = a * w_mul_3;
			int base_b = b * w_mul_3;
			for (int i = 0; i < w_mul_3; i++) {
				int i_a = base_a + i;
				int i_b = base_b + i;
				unsigned char t = buffer[i_b];
				buffer[i_b] = buffer[i_a];
				buffer[i_a] = t;
			}
		}

		cout << "screen pixels read!" << endl;
		
		// 2. generate some image
		vector<unsigned char> image;
		image.resize(w * h * 4);
		for (unsigned y = 0; y < h; y++)
			for (unsigned x = 0; x < w; x++) {
				image[4 * w * y + 4 * x + 0] = buffer[3 * w * y + 3 * x + 0];
				image[4 * w * y + 4 * x + 1] = buffer[3 * w * y + 3 * x + 1];
				image[4 * w * y + 4 * x + 2] = buffer[3 * w * y + 3 * x + 2];
				image[4 * w * y + 4 * x + 3] = 255;
			}

		// 3. file name: current time
		// current date/time based on current system
		const time_t now = time(0);
		// convert now to string form
		tm *ltm = localtime(&now);

		char filename[50];
		float year = ltm->tm_year;
		float month = ltm->tm_mon;
		float day = ltm->tm_mday;
		float hour = ltm->tm_hour;
		float minute = ltm->tm_min;
		float second = ltm->tm_sec;
		sprintf(filename, "../../screenshot/%4.f%2.f%2.f-%2.f%2.f%2.f.png", year, month, day, hour, minute, second);

		// 4. store to PNG file
		vector<unsigned char> png;
		lodepng::State state; //optionally customize this one

		unsigned error = lodepng::encode(png, image, w, h, state);
		if (!error) lodepng::save_file(png, filename);

		//if there's an error, display it
		if (error) cout << "encoder error " << error << ": " << lodepng_error_text(error) << endl;
	}
}



////////////////////////////////////////////////////////////
/// compute spot light matrices from inputs
////////////////////////////////////////////////////////////

glm::vec3 lightPos = glm::vec3(0.f, 1.f, 1.f);
glm::vec3 lightDir;
glm::vec3 lightUp;
glm::vec3 lightRight;

float lightHorizontalAngle = 3.14f;
float lightVerticalAngle = 0.f;
float lightInitialFoV = 90.0f;

glm::vec3 getLightRight() {
	return lightRight;
}
glm::vec3 getLightPosition() {
	return lightPos;
}
glm::vec3 getLightUp() {
	return lightUp;
}
glm::vec3 getLightInvDirection() {
	return -lightDir;
}
float getLightFoV() {
	return lightInitialFoV;
}

glm::mat4 LightViewMatrix;
glm::mat4 LightProjectionMatrix;

glm::mat4 getLightProjectionMatrix() {
	return LightProjectionMatrix;
}
glm::mat4 getLightViewMatrix() {
	return LightViewMatrix;
}

float lightMoveSpeed = 5.0f;
float lightRotateSpeed = 0.5f;

void updateLightDirections() {

	lightDir = glm::vec3(
		cos(lightVerticalAngle) * sin(lightHorizontalAngle),
		sin(lightVerticalAngle),
		cos(lightVerticalAngle) * cos(lightHorizontalAngle)
		);
	lightRight = glm::vec3(
		sin(lightHorizontalAngle - 3.14f / 2.0f),
		0,
		cos(lightHorizontalAngle - 3.14f / 2.0f)
		);
	lightUp = glm::cross(glm::normalize(lightRight), glm::normalize(lightDir));
}

void computeSpotLightMatricesFromInputes() {

	// -----------------------------------
	// print
	// -----------------------------------
	/*
	if (glfwGetKey(window, GLFW_KEY_1) == GLFW_PRESS) {
		printf("lightHorizontalAngle: %f\n", lightHorizontalAngle);
		printf("lightVerticalAngle: %f\n", lightVerticalAngle);
		printf("lightPos: %f, %f, %f\n", lightPos.x, lightPos.y, lightPos.z);
	} //*/

	// glfwGetTime is called only once, the first time this function is called
	static double lastTime = glfwGetTime();

	// Compute time difference between current and last frame
	double currentTime = glfwGetTime();
	float deltaTime = float(currentTime - lastTime);

	// ---------------------------------
	// change angle
	// ---------------------------------
	// Get mouse position
	static double lastXPos = 1024 / 2, lastYPos = 768 / 2;
	double xpos, ypos;
	glfwGetCursorPos(window, &xpos, &ypos);

	if (glfwGetMouseButton(window, GLFW_MOUSE_BUTTON_LEFT) == GLFW_PRESS) {

		lightHorizontalAngle += 0.001f * float(lastXPos - xpos);
		lightVerticalAngle += 0.001f * float(lastYPos - ypos);

		updateLightDirections();
	}

	static bool initialized = false;
	if (!initialized) {
		updateLightDirections();
		initialized = true;
	}


	// ---------------------------------
	// change position
	// ---------------------------------

/*	// w: move forward
	if (glfwGetKey(window, GLFW_KEY_W) == GLFW_PRESS){
		lightPos += lightDir * deltaTime * lightMoveSpeed;
	}
	// s: move backward
	if (glfwGetKey(window, GLFW_KEY_S) == GLFW_PRESS){
		lightPos -= lightDir * deltaTime * lightMoveSpeed;
	} */
	// d: strafe right
	if (glfwGetKey(window, GLFW_KEY_D) == GLFW_PRESS){
		lightPos += getLightRight() * deltaTime * lightMoveSpeed;
	}
	// a: strafe left
	if (glfwGetKey(window, GLFW_KEY_A) == GLFW_PRESS){
		lightPos -= getLightRight() * deltaTime * lightMoveSpeed;
	}
	// w: move up
	if (glfwGetKey(window, GLFW_KEY_W) == GLFW_PRESS){
		lightPos += lightUp * deltaTime * lightMoveSpeed;
	}
	// s: move down
	if (glfwGetKey(window, GLFW_KEY_S) == GLFW_PRESS){
		lightPos -= lightUp * deltaTime * lightMoveSpeed;
	}


	// ---------------------------------
	// update matrices
	// ---------------------------------

	LightProjectionMatrix = glm::perspective<float>(lightInitialFoV, 1.0f, 2.f, 200.0f);
	LightViewMatrix = glm::lookAt(lightPos, lightPos + lightDir, lightUp);

	// For the next frame, the "last time" will be "now"
	lastTime = currentTime;
	lastXPos = xpos;
	lastYPos = ypos;
}

////////////////////////////////////////////////////////////
/// compute camera matrices from inputs
////////////////////////////////////////////////////////////

glm::mat4 ViewMatrix;
glm::mat4 ProjectionMatrix;

glm::mat4 getViewMatrix(){
	return ViewMatrix;
}
glm::mat4 getProjectionMatrix(){
	return ProjectionMatrix;
}

// Initial position : on +Z
glm::vec3 position = glm::vec3(0.f, 0.f, 10.f);
// Initial horizontal angle : toward -Z
float horizontalAngle = 3.14f;
// Initial vertical angle : none
float verticalAngle = 0.f;
// Initial Field of View
float initialFoV = 45.0f;

float speed = 15.f; // 3 units / second

void computeMatricesFromInputs() {
	/*
	if (glfwGetKey(window, GLFW_KEY_2) == GLFW_PRESS) {
		printf("cameraHorizontalAngle: %f\n", horizontalAngle);
		printf("cameraVerticalAngle: %f\n", verticalAngle);
		printf("cameraPos: %f, %f, %f\n", position.x, position.y, position.z);
	} // */

	// glfwGetTime is called only once, the first time this function is called
	static double lastTime = glfwGetTime();

	// Compute time difference between current and last frame
	double currentTime = glfwGetTime();
	float deltaTime = float(currentTime - lastTime);

	// Get mouse position
	static double lastXPos = 1024 / 2, lastYPos = 768 / 2;
	double xpos, ypos;
	glfwGetCursorPos(window, &xpos, &ypos);

	// Reset mouse position for next frame
	//	glfwSetCursorPos(window, 1024 / 2, 768 / 2);
	glfwSetCursorPos(window, xpos, ypos);

	static glm::vec3 direction;
	static glm::vec3 right;
	static glm::vec3 up;

	static bool initialized = false;
	if (!initialized) {
		// Compute new orientation
		//	horizontalAngle += mouseSpeed * float(1024 / 2 - xpos);
		//	verticalAngle += mouseSpeed * float(768 / 2 - ypos);
		horizontalAngle += 0.01f * float(lastXPos - xpos);
		verticalAngle += 0.01f * float(lastYPos - ypos);

		// Direction : Spherical coordinates to Cartesian coordinates conversion
		direction = glm::vec3(
			cos(verticalAngle) * sin(horizontalAngle),
			sin(verticalAngle),
			cos(verticalAngle) * cos(horizontalAngle)
			);

		// Right vector
		right = glm::vec3(
			sin(horizontalAngle - 3.14f / 2.0f),
			0,
			cos(horizontalAngle - 3.14f / 2.0f)
			);

		// Up vector
		up = glm::cross(right, direction);

		initialized = true;
	}

	if (glfwGetMouseButton(window, GLFW_MOUSE_BUTTON_RIGHT) == GLFW_PRESS) {
		// Compute new orientation
		//	horizontalAngle += mouseSpeed * float(1024 / 2 - xpos);
		//	verticalAngle += mouseSpeed * float(768 / 2 - ypos);
		horizontalAngle += 0.001f * float(lastXPos - xpos);
		verticalAngle += 0.001f * float(lastYPos - ypos);

		// Direction : Spherical coordinates to Cartesian coordinates conversion
		direction = glm::vec3(
			cos(verticalAngle) * sin(horizontalAngle),
			sin(verticalAngle),
			cos(verticalAngle) * cos(horizontalAngle)
			);

		// Right vector
		right = glm::vec3(
			sin(horizontalAngle - 3.14f / 2.0f),
			0,
			cos(horizontalAngle - 3.14f / 2.0f)
			);

		// Up vector
		up = glm::cross(right, direction);
	}

	// Move forward
	if (glfwGetKey(window, GLFW_KEY_UP) == GLFW_PRESS){
		position += direction * deltaTime * speed;
	}
	// Move backward
	if (glfwGetKey(window, GLFW_KEY_DOWN) == GLFW_PRESS){
		position -= direction * deltaTime * speed;
	}
	// Strafe right
	if (glfwGetKey(window, GLFW_KEY_RIGHT) == GLFW_PRESS){
		position += right * deltaTime * speed;
	}
	// Strafe left
	if (glfwGetKey(window, GLFW_KEY_LEFT) == GLFW_PRESS){
		position -= right * deltaTime * speed;
	}

	float FoV = initialFoV;// - 5 * glfwGetMouseWheel(); // Now GLFW 3 requires setting up a callback for this. It's a bit too complicated for this beginner's tutorial, so it's disabled instead.

	// Projection matrix : 45?Field of View, 4:3 ratio, display range : 0.1 unit <-> 100 units
//	ProjectionMatrix = glm::perspective(FoV, 4.0f / 3.0f, 0.1f, 100.0f);
	ProjectionMatrix = glm::perspective(FoV, 4.0f / 3.0f, 0.05f, 100.0f);
	// Camera matrix
	ViewMatrix = glm::lookAt(
		position,           // Camera is here
		position + direction, // and looks here : at the same position, plus "direction"
		up                  // Head is up (set to 0,-1,0 to look upside-down)
		);

	// For the next frame, the "last time" will be "now"
	lastTime = currentTime;
	lastXPos = xpos;
	lastYPos = ypos;
}